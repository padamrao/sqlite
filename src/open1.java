import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class open1
{
  public static void main( String args[] )
  {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:B:\\test2.db");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      String sql = "CREATE TABLE sql1 " +
    		  "(ID INT PRIMARY KEY     NOT NULL," +
              " NAME           TEXT    NOT NULL, " + 
              " PASSWORD       TEXT     NOT NULL)" ;
      
      stmt.executeUpdate(sql);
      stmt.close();

      
      String sql2 = "INSERT INTO sql1 (ID,NAME,PASSWORD) " +
                   "VALUES (1, 'Padam','abcd');"; 
      stmt.executeUpdate(sql2);


      stmt.close();
      c.commit();
      c.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    System.out.println("Table and Records created successfully");
  }
}